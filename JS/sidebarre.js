const elements = document.getElementsByClassName('onglet');

// Au depart:
for (let i = 0; i < elements.length; i++) {

    elements[i].children[1].style.display = "none";

}

for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener('click', function(e) {
        e.preventDefault();

        if (this.children[1].style.display === "none") {
            this.children[1].style.display = "block";
            this.children[0].children[1].classList = ["arrow-down"];
        } else {
            this.children[1].style.display = "none";
            this.children[0].children[1].classList = ["arrow-right"];
        }
    })
}