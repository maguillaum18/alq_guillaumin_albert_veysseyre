let burger_button = document.getElementById("btn_burger");
let nav_to_hide = document.getElementById("side-barre");



burger_button.addEventListener('click', function(e) {

    e.preventDefault();

    if (nav_to_hide.className == "show") {
        nav_to_hide.className = "hide";
        burger_button.className = "burger_menu";
    } else {
        nav_to_hide.className = "show";
        burger_button.className = "cross";
    }
})