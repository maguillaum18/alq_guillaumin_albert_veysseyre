<?php
// Connexion à la base de données
$serveur = "localhost";
$utilisateur = "root";
$mot_de_passe = "";
$base_de_donnees = "sportif";
$connexion = mysqli_connect($serveur, $utilisateur, $mot_de_passe, $base_de_donnees);

// Vérification de la connexion à la base de données
if (!$connexion) {
    die("Connexion échouée: " . mysqli_connect_error());
}

// Récupération du mois depuis la requête HTTP GET
if (isset($_GET['month'])) {
    $month = $_GET['month'];
} else {
    $month = date('n'); // Mois en cours
}

// Exécution de la requête SQL
$sql = "SELECT date, distance, time FROM session_train WHERE username = 'davidhill@isima.fr' AND MONTH(date) = $month";
$result = mysqli_query($connexion, $sql);

// Stockage des résultats dans un tableau
$data = array();
while ($row = mysqli_fetch_array($result)) {
    $duree = strtotime($row['time']) - strtotime('00:00:00');
    $temps_en_secondes = intval($duree);
    $temps_en_heures = $temps_en_secondes / 3600;
    $vitesse_moyenne = $row['distance'] / $temps_en_heures;
    $data[] = array('date' => $row['date'], 'distance' => $row['distance'], 'vitesse' => $vitesse_moyenne);
}

// Fermeture de la connexion à la base de données
mysqli_close($connexion);

// Renvoi des données au format JSON
echo json_encode($data);
?>
