<?php
session_start();






/************** Récupération des identifiants ****************/

if (!isset($_POST['username']) || !isset($_POST['password'])) {

    $_SESSION["error"] = "Veuillez entrer un mail et un mot de passe";
    header("Location: connexion.php");
    die();
}


/************** Validation des identifiants *****************/

//Connexion à la base de données
include("./BDD/database.php");

try {
    // mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT); // DEBUG

    $mysqli = new mysqli($host, $admin, $password_admin, $db_name);

    /************** Vérification des données ********************/
    // Sécurité
    $username = filter_input(INPUT_POST, 'username');
    $password = filter_input(INPUT_POST, 'password');
    $username = htmlspecialchars($username);
    $password = htmlspecialchars($password);
    $username = mysqli_real_escape_string($mysqli, $username);
    $password = mysqli_real_escape_string($mysqli, $password);

    //Si correcte on peut passer à la phase suivante sinon on le vire du site
    if ($username != $_POST['username'] || $_POST['password'] != $password) {
        $mysqli->close();
        header("Location: prisonDeLaMuerte.html");
        die();
    }

    $sqlQuery = "SELECT * FROM login WHERE username= ?";
    $stmt = $mysqli->prepare($sqlQuery);
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result()->fetch_assoc();
    $password_base = $result["password"];
    mysqli_stmt_close($stmt);
} catch (Exception $e) {
    $_SESSION["error"] = "impossible de se connecter à la bdd";
    die('Erreur : ' . $e->getMessage());
}



if ($password != $password_base) {
    $_SESSION["error"] = "Mauvais  mot de passe";
    header("Location: connexion.php");
    die("Mauvais mot de passe");
}

//Si valide => rediriger vers index.html
$_SESSION['is_logged'] = true;
$_SESSION['username'] = $username;



header("Location: index.php");
die("Probleme redirection");

?>
