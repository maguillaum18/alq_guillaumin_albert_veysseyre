<?php
session_start();

if (!isset($_SESSION["error"])) {
    $_SESSION["error"] = "";
}

if (isset($_SESSION["is_logged"]) && $_SESSION["is_logged"] === true) {
    header("Location: index.php");
}

?>



<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Connexion</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/sign-in/">
    <link href="CSS/reset.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <meta name="theme-color" content="#712cf9">
    <link rel="stylesheet" href="CSS/connexion.css">
    <style>
        .red {
            color: red;
        }
    </style>
</head>

<body class="text-center" style="height: 625px; display: flex;
flex-direction: column;
align-items: center;">
    <main class="form-signin w-100 m-auto">

        <form style="width: 400px; margin: auto;" method="post" action="newUser.php" id="register_form">
            <h1 class="h3 mb-3 fw-normal">S'inscrire</h1>
            
            <div class="form-floating">
                <input type="nom" class="form-control" name="nom" id="nom" placeholder="Nom">
                <label for="identifier">Nom</label>
            </div>
            
            <div class="form-floating">
                <input type="prenom" class="form-control" name="prenom" id="prenom" placeholder="Prénom">
                <label for="identifier">Prénom</label>
            </div>

            <div class="form-floating">
                <input type="email" class="form-control" name="username" id="username" placeholder="name@example.com">
                <label for="identifier">Adresse mail</label>
            </div>
            
            <div class="form-floating">

                <input minlength="5" type="password" class="form-control" name="password" id="password"
                    placeholder="Password">
                <label for="password">Mot de passe</label>
            </div>



            <button class="w-100 btn btn-lg btn-primary"  type="submit" id="xyz" >Inscription</button>
        </form>
        
        
        <div id="error">
            <p class="red">
                <?php echo $_SESSION["error"]; ?>
            </p>
        </div>
        
        
    </main>
    <script type="text/javascript" src="JS/inscription.js"></script>
</body>

</html>