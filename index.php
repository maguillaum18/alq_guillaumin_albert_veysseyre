 <?php
session_start();
if (!isset($_SESSION['is_logged']) || $_SESSION['is_logged'] != true) {
  header('Location: connexion.php');
  die();
}






?>

<!doctype html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="CSS/reset.css">
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <link rel="stylesheet" href="CSS/index.css">
  <link rel="stylesheet" href="CSS/top-barre.css">
  <link rel="stylesheet" href="CSS/side-barre.css">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

  <title>Accueil</title>
</head>

<body>
  <?php include("topbarre.html"); ?>
  <?php include("sidenav.html"); ?>
  <?php include("hello.php"); ?>

  <main>


            <div class="coucou">
             <h1>  <?php
             hello();
             echo "Bonjour " . $_SESSION['firstname'] . " " . $_SESSION['name']; ?> </h1>
           </div> </br>

  	                 <form method="post" action="ajouter_entrainement.php" class="session-form">
                       <h2>Ajouter une session d'entraînement</h2>
  		                   <label for="date">Date :</label>
		                     <input type="date" id="date" name="date" required>
		                      <label for="duree">Durée :</label>
		                      <input type="time" id="duree" name="duree" required>
		                      <label for="distance">Distance :</label>
		                      <input type="number" id="distance" name="distance" required>
		                      <input type="submit" value="Ajouter la session d'entraînement">
                  </form>



                   <div id="chart_container">
                     <div>
                        <label for="month-select">Mois :</label>
                        <select id="month-select">
                          <option value="1">Janvier</option>
                          <option value="2">Février</option>
                          <option value="3">Mars</option>
                          <option value="4">Avril</option>
                          <option value="5">Mai</option>
                          <option value="6">Juin</option>
                          <option value="7">Juillet</option>
                          <option value="8">Août</option>
                          <option value="9">Septembre</option>
                          <option value="10">Octobre</option>
                          <option value="11">Novembre</option>
                          <option value="12">Décembre</option>
                        </select>
                      </div>
                     <canvas id="myChart"></canvas>
                   </div>








    </main>

<!-- Inclure les fichiers JavaScript pour la bibliothèque Chart.js et le script pour le graphique -->

    <script src="JS/top-navbarre.js"></script>
    <script src="JS/sidebarre.js"></script>
    <script src="JS/datarequest.js"></script>
    <script src="graph.js?<?php echo rand(); ?>"></script>
    <script src="graph.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>


</body>

</html>
