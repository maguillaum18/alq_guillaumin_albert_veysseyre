<?php
session_start();
$_SESSION["error"] = "";

/************** Validation des identifiants *****************/

//Connexion à la base de données
include("./BDD/database.php");

try {
    // mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT); // DEBUG

    $mysqli = new mysqli($host, $admin, $password_admin, $db_name);

    $username = $_POST['username'];
    $password = $_POST['password'];
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $query = 'SELECT * FROM login WHERE username = $username';
    $result = $mysqli->query($query);
    $_SESSION["error"] = $result->num_rows;

    if($result->num_rows == 0){
        /*$query = 'INSERT INTO login (id, username, password) VALUES (NULL,' . $username . ',' . $password . ')';
        $mysqli->query($query);*/
        $stmt = $mysqli->prepare('INSERT INTO login (id, username, password, name, firstname) VALUES (NULL, ?, ?, ?, ?)');
        $stmt->bind_param("ssss", $username, $password, $nom, $prenom);
        $stmt->execute();
        $stmt->close();
        $_SESSION['is_logged'] = true;
        $_SESSION['username'] = $username;
        header("Location: index.php");
    }
    else {
        //$_SESSION["error"] = "Utilisateur déjà existant";
        header("Location: inscription.php");
    }
} catch (Exception $e) {
    $_SESSION["error"] = "impossible de se connecter à la bdd";
    die('Erreur : ' . $e->getMessage());
}

die("Probleme redirection");

?>
