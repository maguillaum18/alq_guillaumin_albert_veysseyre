// Fonction pour récupérer les données via une requête AJAX
function getData(month) {
  return fetch(`get_data.php?month=${month}`)
    .then(response => response.json())
    .catch(error => {
      console.log('Erreur lors de la récupération des données : ' + error.message);
    });
}

// Fonction pour dessiner le graphique avec les données
function drawChart(data) {
  let labels = [];
  let distances = [];
  let vitesses = [];

  for (let i = 0; i < data.length; i++) {
    labels.push(data[i].date);
    distances.push(data[i].distance);
    vitesses.push(data[i].vitesse);
  }

  let ctx = document.getElementById('myChart').getContext('2d');
  let chart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: labels,
      datasets: [{
        label: 'Distance parcourue (km)',
        data: distances,
        backgroundColor: 'rgba(0, 119, 204, 0.3)',
        borderColor: 'rgba(0, 119, 204, 1)',
        borderWidth: 1,
        pointRadius: 3,
        pointBackgroundColor: 'rgba(0, 119, 204, 1)',
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(0, 119, 204, 1)'
      },
      {
        label: 'Vitesse moyenne (km/h)',
        data: vitesses,
        type: 'line',
        backgroundColor: 'rgba(255, 99, 132, 0.3)',
        borderColor: 'rgba(255, 99, 132, 1)',
        borderWidth: 1,
        pointRadius: 3,
        pointBackgroundColor: 'rgba(255, 99, 132, 1)',
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(255, 99, 132, 1)'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
}

// Fonction pour mettre à jour le graphique avec de nouvelles données
function updateGraph(month) {
  getData(month).then(function (data) {
    let ctx = document.getElementById('myChart').getContext('2d');
    ctx.canvas.parentNode.removeChild(ctx.canvas);
    let canvas = document.createElement('canvas');
    canvas.id = 'myChart';
    document.getElementById('chart_container').appendChild(canvas);
    drawChart(data);
  }).catch(function (error) {
    console.log('Erreur lors de la récupération des données : ' + error.message);
  });
}

// Récupération du select
let monthSelect = document.getElementById('month-select');

// Écouteur d'événement pour mettre à jour le graphique lorsqu'une nouvelle option est sélectionnée
monthSelect.addEventListener('change', function () {
  const month = document.getElementById('month-select').value;
  updateGraph(month);
});
